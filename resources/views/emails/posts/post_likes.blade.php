@component('mail::message')
# Your Post was liked

{{$liker->name}} liked one of your post

@component('mail::button', ['url' => route('posts.show')])
View Post
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
