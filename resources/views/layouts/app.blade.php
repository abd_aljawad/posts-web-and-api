<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Posty</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body class="bg-gray-200">
<nav class="p-6 bg-white flex justify-between mb-6">
    <ul class="flex items-center">
        <li class="p-3">
            <a href="/">Home</a>
        </li>
        <li class="p-3">
            <a href="{{route('dashboard')}}">Dashboard</a>
        </li>
        <li class="p-3">
            <a href="{{route('posts')}}">Post</a>
        </li>
    </ul>
    <ul class="flex items-center">
        @if(auth()->user())  {{-- or @auth--}}
            <li>
                <a href="" class="p-3">{{auth()->user()->name}}</a>
            </li>
            <li>
                <form action="{{route('logout')}}" method="post" class="p-3 inline">
                    @csrf
                    <button type="submit" >Logout</button>
                </form>
            </li>
        {{-- or @endauth--}}
        @else    {{-- or @guest--}}
            <li>
                <a href="{{ route('login') }}" class="p-3">Login</a>
            </li>
            <li>
                <a href="{{ route('register') }}" class="p-3">Register</a>
            </li>
            @endif     {{-- or @endguest--}}



    </ul>
</nav>
@yield('content')
</body>
</html>
