@extends('layouts.app')
@section('content')
    <div class="flex justify-center">
        <div class="w-8/12 bg-white p-6 rounded-lg">
            @auth()
            <form enctype="multipart/form-data" action="{{route('posts')}}" method="post">
                @csrf
                <div class="mb-4">
                    <label for="body" class="sr-only">Body</label>
                    <textarea name="body" id="body" cols="30" rows="4"
                              class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('body') border-red-500 @enderror"
                              placeholder="Post SomeThing !"></textarea>
                    @error('body')
                    <div class="text-red-500 mt-2 text-sm">
                        {{$message}}
                    </div>
                    @enderror
                </div>

                <div class="mb-4">
                    <label for="body" class="sr-only">Body</label>
                    <input type="file" name="file" id="file"
                              class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('file') border-red-500 @enderror">
                    @error('file')
                    <div class="text-red-500 mt-2 text-sm">
                        {{$message}}
                    </div>
                    @enderror
                </div>
                <div class="mb-4 ">
                    <label for="price" >Price</label>
                    <input type="number" name="price" id="price" value="100"
                           class="bg-gray-100 border-2 w-4/12 py-2 rounded-lg @error('price') border-red-500 @enderror">
                    $
                    @error('price')
                    <div class="text-red-500 mt-2 text-sm">
                        {{$message}}
                    </div>
                    @enderror
                </div>
                <div>
                    <button type="submit" class="bg-blue-500 text-white px-4 py-2 rounded font-medium"> Post</button>
                </div>
            </form>
            @endauth
            @if(count($posts))
                @foreach($posts as $post)
                    <div class="mb-4 mt-4">
                        <a href="{{route('users.posts',$post->user)}}" class="font-bold">{{$post->user->name}} </a> <span
                            class="text-gray-600 text-sm">{{$post->created_at}}</span>
                        <div class="font-bold"> Price: {{$post->price}}$</div>
                        @if($post->file_url)

                            <div class="mt-4 mb-4 card">
                                <img src="{{ $post->file_url }}" alt="post_image" />
                            </div>

                        @endif
                        <p class="mb-2">{{$post->body}}</p>

                        @can('delete',$post)
                           <form action="{{route('posts.destroy',$post)}}" method="post">

                               @csrf
                               @method('DELETE')
                               <button type="submit" class="text-blue-500">Delete</button>
                           </form>
                        @endcan


                        <div class="flex items-center">
                            @auth()
                                @if(!$post->likedBy(auth()->user()))
                                    <form action="{{route('posts.likes',$post)}}" method="post" class="mr-1">
                                        @csrf
                                        <button type="submit" class="text-blue-500">Like</button>
                                    </form>
                                @else
                                    <form action="{{route('posts.likes',$post)}}" method="post" class="mr-1">
                                        @method('DELETE')
                                        @csrf
                                        <button type="submit" class="text-blue-500">Unlike</button>
                                    </form>
                                @endif

                            @endauth
                            <span> {{ $post->likes->count() }} {{\Illuminate\Support\Str::plural('like',$post->likes->count())}} </span>
                        </div>
                    </div>

                @endforeach
                {{$posts->links()}}
            @else
                <div class="text-center"> There are no posts yet</div>
            @endif
        </div>
    </div>
@endsection
