<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\LogoutController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\PostController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/////////////// steps to deploy your application /////////////////
// ssh -p 65002 u526533852@151.106.96.211
// enter password
// cd public_html/posts
// git clone repository.git
// composer install
// npm install
// add .env file
// php artisan migrate
//////////////////////////////////////////////////////////////////

Route::middleware('auth:sanctum')->group(function () {
    Route::post('/logout',[\App\Http\Controllers\api\AuthController::class,'logout']);
    Route::apiResource('post',\App\Http\Controllers\api\PostController::class);
    Route::get('/profile', [\App\Http\Controllers\api\AuthController::class, 'user_info']);
    Route::Post('/like/{post}',[\App\Http\Controllers\api\PostLikedController::class,'store']);
    Route::delete('/like/{post}',[\App\Http\Controllers\api\PostLikedController::class,'destroy']);
});

Route::post('/register',[\App\Http\Controllers\api\AuthController::class,'register']);
Route::post('/login',[\App\Http\Controllers\api\AuthController::class,'login']);


////////////////////////////////////////////////////////////////////////////////////////////
Route::get('/dashboard',[DashboardController::class,'index'])->name('dashboard');
Route::get('/users/{user}/posts',[\App\Http\Controllers\UserPostController::class,'index'])->name('users.posts');


Route::get('/posts',[PostController::class,'index'])->name('posts');
Route::get('/posts/{post}',[PostController::class,'show'])->name('posts.show');

Route::post('/posts',[PostController::class,'store']);
Route::delete('/posts/{post}',[PostController::class,'destroy'])->name('posts.destroy');
Route::post('/posts/{post}/likes',[\App\Http\Controllers\PostLikeController::class,'store'])->name('posts.likes');
Route::delete('/posts/{post}/likes',[\App\Http\Controllers\PostLikeController::class,'destroy'])->name('posts.likes');


