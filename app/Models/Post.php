<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
class Post extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia;
    protected $fillable=['body', 'price'];
    protected $hidden = ['likes','media', 'updated_at'];

    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->diffForHumans();
    }


    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('post_images')
            ->height(232)

           ;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function  likes()
    {
        return $this->hasMany(Like::class);
    }
    public function  likedBy(User $user)
    {
        return $this->likes->contains('user_id',$user->id);
    }



}
