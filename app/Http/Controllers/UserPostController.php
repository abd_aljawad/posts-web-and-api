<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserPostController extends Controller
{
    public function index(User $user)
    {
        $posts=$user->posts()->latest()->with(['user','likes'])->paginate(5);
        foreach ($posts as $post){
            $post->file_url = $post->getFirstMediaUrl('post_image', 'post_images');
        }
        $data=[
            'user'=>$user,
            'posts'=>$posts
        ];

        return view('users.posts.index',$data);
    }
}
