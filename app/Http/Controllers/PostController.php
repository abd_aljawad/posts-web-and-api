<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth'])->only('store','destroy');
    }

    public function index(){
        $data['posts'] = Post::latest()->with(['user','likes'])->paginate(5);
        foreach ($data['posts'] as $post){
            $post->file_url = $post->getFirstMediaUrl('post_image', 'post_images');
        }

        return view('posts.index', $data);
    }
    public function store(Request $request){
        $this->validate($request,[
            'body'=>'required',
            'price' => 'required',
            'file'=>'image|mimes:jpg,png,jpeg'
        ]);
        $post = $request->user()->posts()->create([
            'body'=>$request->body,
            'price' => $request->price,
        ]);
        $post->addMediaFromRequest('file')->toMediaCollection('post_image');
        return back();

    }
    public function destroy(Post $post){
           $this->authorize('delete',$post);
        $post->delete();
        return back();

    }
    public function show(Post $post)
    {
        return view('posts.show',[
            'post'=>$post
        ]);
    }
}
