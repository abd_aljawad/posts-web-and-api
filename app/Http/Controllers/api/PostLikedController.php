<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\Post;
use Illuminate\Http\Request;

class PostLikedController extends Controller
{
    public function store(Post $post,Request $request){
        if($post->likedBy($request->user()))
        {
            return response(['message' => 'Already liked'], 409);
        }
      $like=  $post->likes()->create([
            'user_id' => $request->user()->id
        ]);

        return response($like);

    }
    public function destroy(Post $post, Request $request){
        $request->user()->likes()->where('post_id',$post->id)->delete();
        return response(['message' => 'Deleted Successfully']);
    }

}
