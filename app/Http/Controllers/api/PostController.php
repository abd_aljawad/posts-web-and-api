<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        $posts = Post::latest()->with(['user'])->withCount('likes')->get();
        foreach ($posts as $post){
            $post['image_url']= $post->getFirstMediaUrl('post_image', 'post_images');
            $post['is_liked'] = $post->likedBy($request->user());
        }
        return response($posts);


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request,[
            'body'=>'required',
            'price' => 'required',
            'file'=>'image|mimes:jpg,png,jpeg',


        ]);
        $post = $request->user()->posts()->create([
            'body'=>$request->body,
            'price' => $request->price,
        ]);
        $post->addMediaFromRequest('file')->toMediaCollection('post_image');
        return response($post, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {

        //$user = User::where('id',$post->user_id)->first();
        $post['likes_count'] = $post->likes->count();
        $post['user'] = $post->user;

        return  response($post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {

        $post->delete();


        return response(['message' => 'Deleted successfully!']);
    }
}
